#include "hitechnic-gyro.h"
#include "hitechnic-irseeker-v2.h"
#include "hitechnic-sensormux.h"
#include "lego-ultrasound.h"
#include "JoystickDriver.c"
#include "lego-light.h"

#define CentiTickV 39 //The amount of tickes per centimeter going forward/back
#define CentiTickH 46 // """"" sideways
#define gyroFluctuation 0.3 // Imprecision in Gyroscope readings
#define LinearTopPosition 8400 //120 cm position
#define LinearMidPosition 5900 //90 cm position
#define LinearLowPosition 3900 //60 cm position
#define openDoor 255
#define closeDoor 115
#define scoop3Up 255
#define scoop3Down 50

#define distEncoder rightb //The encoder that is used to measure distance in movement functions
													 //defined as constant so that we can quickly change it during competition
const tMUXSensor lightSensor = msensor_S1_3;
tHTIRS2 irSeeker;
tHTGYRO gyroSensor;
float orientation;
bool linear_moving=false;
int linear_position; //target position for linear slide, in encoder ticks
int sweeptog = 0;


  ///////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////
  ////                                                   ////
  ////    The Gyro task -- keeping track of the gyro     ////
  ////    maintains global variable orientation          ////
  ////    showing current robot heading realtive to the  ////
  ////    starting position (in degrees,                 ////
  ////       positive=counterclockwise									 ////
  ////																									 ////
  ///////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////

//Keep Track of Gyro

task gyro{
	wait1Msec(2000);
	sensorCalibrate(&gyroSensor);
	while(true){
		time1[T1] = 0;
		readSensor(&gyroSensor);
		orientation += abs(gyroSensor.rotation) > gyroFluctuation ? gyroSensor.rotation * 0.02 : 0;
		while(time1[T1] < 20){wait1Msec(1);}
	}
}
  ///////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////
  ////                                                   ////
  ////    Debug task -- print sensor data to screen      ////
  ////                                                   ////
  ///////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////

task debug{
  while (true) {
  	readSensor(&irSeeker);
		displayCenteredTextLine(0, "Light Reading");       /* Display IR Sensor values */
		displayCenteredBigTextLine(2, "%3d %3d", irSeeker.acValues[1], irSeeker.acValues[2]);
	  displayCenteredBigTextLine(6, "G:%f", orientation);
    wait1Msec(50);
  }
}
  ///////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////
  ////                                                   ////
  ////    Basic driving functions.                       ////
  ////    Distances in all functions are in cm           ////
  ////                                                   ////
  ///////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////


// Stop all wheel motors
void stopMotors() {
	motor[rightf] = 0;
	motor[rightb] = 0;
	motor[leftf] = 0;
	motor[leftb] = 0;
}

void setMotors(int forwardspeed, int rightspeed, int turnspeed ){ // omnidirectional robot motion
	motor[rightf] = forwardspeed - rightspeed + turnspeed;
	motor[rightb] = forwardspeed + rightspeed + turnspeed;
	motor[leftf]  = forwardspeed + rightspeed - turnspeed;
	motor[leftb]  = forwardspeed - rightspeed - turnspeed;
}
float ramp(int distance, int target){
	int ramp_distance=min2(target/5, 300);
	if (distance < 0 || distance > target) {
		return 0;
	}
	if (distance< ramp_distance) {
    return ((float) distance)/((float)ramp_distance);
  } else if (distance < target - ramp_distance) {
  	return 1;
  } else {
  	return abs(((float)(target - distance))/((float)ramp_distance));
  }
}

void goForwardSimple(int speed, int distance) {
	float initialOffset = orientation;
	nMotorEncoder[distEncoder] = 0;
	int target=distance * CentiTickV;
	float correction = 0;

	while(abs(nMotorEncoder[distEncoder]) < target) {
   	correction = (orientation - initialOffset) * 1;
	 	setMotors(speed, 0, -correction );
	}
	stopMotors();
}

void goForward(int speed, int distance) { // forward with Gyro correction
	float initialOffset = orientation;
	nMotorEncoder[distEncoder] = 0;
	int target=distance * CentiTickV;
	float correction = 0;

	while(abs(nMotorEncoder[distEncoder]) < target) {
   	correction = (orientation - initialOffset) * 1;
	 	setMotors((speed - 20)*ramp(nMotorEncoder[distEncoder],target) + 20, 0, -correction );
	}
	stopMotors();
}

void goBackward(int speed, int distance) { // backward with Gyro correction
	float initialOffset = orientation;
	nMotorEncoder[distEncoder] = 0;
	int target=distance * CentiTickV;
	float correction = 0;

	while(abs(nMotorEncoder[distEncoder]) < target) {
   	correction = (orientation - initialOffset) * 1;
	 	setMotors(-((speed - 20)*ramp(abs(nMotorEncoder[distEncoder]),target) + 20), 0, -correction );
	}
	stopMotors();
}

void goLeft(int speed, int distance) { // robot goes left
  float initialOffset = orientation;
	nMotorEncoder[distEncoder] = 0;

	while(abs(nMotorEncoder[distEncoder]) < distance * CentiTickH) {
		readSensor(&gyroSensor);
		setMotors(0, -speed, -gyroSensor.rotation*2 + (initialOffset - orientation) - 15);
	}
	stopMotors();
}

void goRight(int speed, int distance) { // robot goes right
  float initialOffset = orientation;
	nMotorEncoder[distEncoder] = 0;

	while(abs(nMotorEncoder[distEncoder]) < distance * CentiTickH) {
		readSensor(&gyroSensor);
		setMotors(0, speed, -gyroSensor.rotation*2 + (initialOffset - orientation) + 15);
	}
	stopMotors();
}

void turnCW(int velocity, float orientChange) { // counterclockwise turning without lateral motion
	float iorientation = orientation; // speed is expected to be positive but can be negative if you really really truly desire it in your heart
	setMotors(0, 0, velocity);
	while(abs(iorientation - orientation) < orientChange*0.98) {
		displayCenteredBigTextLine(2, "%3d", (iorientation - orientation));
	}
	setMotors(0, 0, 0);
}

void turnCCW(int velocity, float orientChange) { // counterclockwise turning without lateral motion
	float iorientation = orientation; // speed is expected to be positive but can be negative if you really really truly desire it in your heart
	setMotors(0, 0, -velocity);
	while(abs(iorientation - orientation) < orientChange*0.98) {
		displayCenteredBigTextLine(2, "%3d", (iorientation - orientation));
	}
	setMotors(0, 0, 0);
}


  ///////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////
  ////                                                   ////
  ////    Linear slide functions                         ////
  ////                                                   ////
  ///////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////


void setLinear(int speed){ // set linear motion
 if ((nMotorEncoder[linearl]<=0) &&(speed<0)) {
   motor[linearl]=0;
 } else {
   motor[linearl]=speed;
 }
 if ((nMotorEncoder[linearr]<=0) &&(speed<0)) {
   motor[linearr]=0;
 } else {
   motor[linearr]=speed;
 }
}

task positionLinear {
	sweeptog = 0;
  linear_moving=true; // indicate slide is moving

  int distance_to_move=linear_position-nMotorEncoder[linearl]; // set target
  nMotorEncoderTarget[linearl]=distance_to_move;
  nMotorEncoderTarget[linearr]=distance_to_move;
  wait1Msec(10); //wait for communication with the controller to complete
  if (distance_to_move>0) {
     motor[linearl]=70; motor[linearr]=70; // moving up
  } else {
    motor[linearl]=-70; motor[linearr]=-70; // moving down
  }
  while (nMotorRunState[linearl] != runStateIdle ){}
  motor[linearl]=0; motor[linearr]=0;
  linear_moving=false;
 }
