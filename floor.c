#pragma config(Hubs,  S2, HTMotor,  HTMotor,  HTMotor,  HTMotor)
#pragma config(Hubs,  S3, HTServo,  none,     none,     none)
#pragma config(Sensor, S1,     HTSMUX,         sensorI2CCustom)
#pragma config(Sensor, S2,     ,               sensorI2CMuxController)
#pragma config(Sensor, S3,     ,               sensorI2CMuxController)
#pragma config(Motor,  mtr_S2_C1_1,     leftf,         tmotorTetrix, openLoop, reversed)
#pragma config(Motor,  mtr_S2_C1_2,     linearl,       tmotorTetrix, PIDControl, reversed, encoder)
#pragma config(Motor,  mtr_S2_C2_1,     leftb,         tmotorTetrix, openLoop, reversed, encoder)
#pragma config(Motor,  mtr_S2_C2_2,     sweeper,       tmotorTetrix, openLoop, reversed)
#pragma config(Motor,  mtr_S2_C3_1,     rightf,        tmotorTetrix, openLoop)
#pragma config(Motor,  mtr_S2_C3_2,     rightb,        tmotorTetrix, openLoop, encoder)
#pragma config(Motor,  mtr_S2_C4_1,     linearr,       tmotorTetrix, PIDControl, encoder)
#pragma config(Motor,  mtr_S2_C4_2,     motorG,        tmotorTetrix, openLoop)
#pragma config(Servo,  srvo_S3_C1_1,    grabscoop,            tServoStandard)
#pragma config(Servo,  srvo_S3_C1_2,    door,                 tServoStandard)
#pragma config(Servo,  srvo_S3_C1_3,    ultra,                tServoStandard)
#pragma config(Servo,  srvo_S3_C1_4,    grabscoop2,           tServoStandard)
#pragma config(Servo,  srvo_S3_C1_5,    grabscoop3,           tServoStandard)
#pragma config(Servo,  srvo_S3_C1_6,    servo6,               tServoStandard)
//*!!Code automatically generated by 'ROBOTC' configuration wizard               !!*//

/* connections:
NXT S1->Multiplexer
1->sonarSensor
2->irSeeker
S2->MotorController1->MC2->MC3->MC4
S3->ServoController
S4->Gyro sensor
*/
#define APPROACH_TIME 2000

#include "BasicFunctions.h"
/* contains the following functions
stopMotors();
setMotors(forward, right, turn);
goForward(speed, distance);
goLeft(speed, distance);
goRight(speed, distance);
turnInPlace(speed, degrees);
setLinear(speed);
posiitonLinear(position);
also contains definitions of Sonarsensor, IR seeker,   and the task gyro
*/

bool irsearch = false;

// Check if IR threshold for determining if the robot in the correct position with respect to the IR beacon
bool checkIR() {
	readSensor(&irSeeker);
	return ((irSeeker.acValues[1] > 40) && (irSeeker.acValues[2] > 40));
}

task hex() {
	irsearch = true;
	goLeft(90, 40);
	irsearch = false;
	goLeft(90, 25);
	turnCCW(80, 50);
	goLeft(90, 40);
	irsearch = true;
	goLeft(90, 13);
	irsearch = false;
	goLeft(90, 37);
	turnCCW(80, 50);
	goLeft(90, 15);
	irsearch = true;
	goLeft(90, 80);
}

void approachBeacon() { // Comeon read the friggin function title jesus christ I don't have to spell it out for you
	time1[T2] = 0;
	float irHorizRatio;
	while(time1[T2] < APPROACH_TIME) {
   	readSensor(&irSeeker);
		irHorizRatio = (float)(irSeeker.acValues[2] - irSeeker.acValues[1])/((float)(irSeeker.acValues[1] + irSeeker.acValues[2]));
	 	setMotors(25, irHorizRatio*37, 0);
	}
	stopMotors();
}

//Initialization of all the servos etc.
void initializeRobot()
{
	initSensor(&gyroSensor, S4);
	startTask(gyro);
	nMotorEncoder[linearl]=0;	nMotorEncoder[linearr]=0;
	servo[door] = closeDoor;
	servo[grabscoop3] = 255;
	initSensor(&irSeeker, msensor_S1_2);
	LSsetActive(lightSensor);
	wait1Msec(5000);
}

///////////////////////////////////////////////
///////////////////////////////////////////////
////                                       ////
////    Beginning of the main program      ////
////                                       ////
///////////////////////////////////////////////
///////////////////////////////////////////////

task main() {
	initializeRobot();
	bool bypass	=	false;
  float delay = 0;
  clearTimer(T2);
  playSound (soundUpwardTones);

  string wfs = "WFS";
  while(true) {
  	nxtDisplayCenteredBigTextLine(3, wfs);
  	if (nNxtButtonPressed == 3) {
  		wait1Msec(500);
  		break;
  	}
  	if (nNxtButtonPressed == 1) {
  		wfs = "nWFS";
  		bypass = false;
  	}
  	if (nNxtButtonPressed == 2) {
  		wfs = "WFS";
  		bypass = true;
  	}
	}

  eraseDisplay();
  playSound (soundFastUpwardTones);
  wait1Msec(500);
  // get delay value
  while (true) {
    nxtDisplayCenteredBigTextLine(3, "%3.1f",delay);
    if (nNxtButtonPressed == 3) {
  	wait1Msec(500);
  		break;
    }
    if (nNxtButtonPressed == 2 && delay != 0) {
  	delay = delay - 0.5;
 	 	wait1Msec(250);
    }
    if(nNxtButtonPressed == 1) {
 		delay = delay + 0.5;
 		 	wait1Msec(250);
    }
  }
  if(bypass) {
  	waitForStart();
  }
  wait1Msec(delay*1000);

	goForward(100, 70); // approach center structure

	startTask(hex); // search for IR sensor
	while(!(checkIR() && irsearch)){}
	stopTask(hex);
	setMotors(0, 0, 0);
	wait1Msec(200);

	approachBeacon(); // get to beacon
	setMotors(100, 0, 0);
	wait1Msec(300);
	setMotors(0, 0, 0);

	goBackward(40, 10);

  linear_position=LinearTopPosition; // raise linear slide
	startTask(positionLinear);
	wait1Msec(200);
	while (linear_moving) {}
	goForward(27, 3.8);
	wait1Msec(1000);

	servo[door] = openDoor; // drop the ball
	wait1Msec(1400);
	goBackward(40, 15);
  servo[door] = closeDoor;

	linear_position = 200;
	startTask(positionLinear);

	turnCCW(90, 73);wait1Msec(100);
	goForward(50, 57);wait1Msec(100);
	turnCCW(90, 77);wait1Msec(100);
	goBackward(100, 120);wait1Msec(100);
	turnCCW(90, 73);
  stopMotors();
}
